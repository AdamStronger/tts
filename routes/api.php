<?php

use App\Http\Controllers\AliexpressController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::any('/user', function (Request $request) {
    return $request->user();
});
