<?php

use App\Http\Controllers\AliexpressController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\ShopifyWebHooks;
use App\Http\Controllers\ShopifyWebHooks2;
use App\Http\Controllers\ShopifyWebHooks3;
use App\Http\Controllers\ShopifyWebHooks4;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GoogleSheetsController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/app', function () {
    return view('app.welcome');
})->middleware(['verify.shopify'])->name('app.home');

Route::get('/app/products', [productsController::class, 'list'])->middleware(['verify.shopify'])->name('app.products');
Route::get('/app/mapproduct', [productsController::class, 'map'])->middleware(['verify.shopify'])->name('app.mapProduct');

Route::get('/app/setting', [settingController::class, 'show'])->middleware(['verify.shopify'])->name('app.setting');


Route::get('/app/aliexpress/{id}', [AliexpressController::class, 'connect'])->name('Aliconnect');
Route::get('/app/aliexpressAccess/{id}', [AliexpressController::class, 'getCode'])->name('AligetCode');
Route::post('/app/aliproduct', [AliexpressController::class, 'getproduct'])->name('aliproduct');
Route::post('/app/mapProduct', [AliexpressController::class, 'mapProduct'])->name('mapProduct');

Route::post('/app/Jokersync', [ShopifyWebHooks::class, 'order_created'])->name('order_created');
Route::post('/app/x15sync', [ShopifyWebHooks2::class, 'order_created'])->name('order_created');
Route::post('/app/dlb', [ShopifyWebHooks3::class, 'order_created'])->name('order_created');
Route::post('/app/leves', [ShopifyWebHooks4::class, 'order_created'])->name('order_created');


Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';
