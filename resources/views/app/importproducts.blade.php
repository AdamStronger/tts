@extends('shopify-app::layouts.default')

@section('styles')
    @vite(['resources/css/app.css','resources/js/app.js'])
@endsection

@section('content')
    <div class="bg-gray-50 dark:bg-gray-800">
        @include('app.layout.appNav')
        <div class="flex pt-16 overflow-hidden bg-gray-50 dark:bg-gray-900">
            <div class="relative w-full max-w-screen-2xl mx-auto h-full overflow-y-auto bg-gray-50 dark:bg-gray-900">
                <main>
                    {{var_dump($data)}}
            </div>
            </main>
        </div>
    </div>
@endsection
