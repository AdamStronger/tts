@extends('shopify-app::layouts.default')

@section('styles')
    @vite(['resources/css/app.css','resources/js/app.js'])
@endsection

@section('content')
    <div class="bg-gray-50 dark:bg-gray-800">
        @include('app.layout.appNav')
        <div class="flex pt-16 overflow-hidden bg-gray-50 dark:bg-gray-900">
            <div class="relative w-full max-w-screen-2xl mx-auto h-full overflow-y-auto bg-gray-50 dark:bg-gray-900">
                <main>
                    <div class="px-4 pt-6 2xl:px-0">
                        <div href="#" class="btn ml-6 lg:mr-0 lg:mb-6"> Accounts</div>
                        <a target="_blank"
                           href="{{ url('app/aliexpress/' . Auth::user()->id) }}"
                           @if($isAliConnected)
                               class="text-gray-900 bg-white hover:bg-gray-100 border border-gray-200 focus:ring-4 focus:outline-none focus:ring-gray-100 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:focus:ring-gray-600 dark:bg-gray-800 dark:border-gray-700 dark:text-white dark:hover:bg-gray-700 me-2 mb-2"
                                onclick="return false;"
                           @else
                               class="text-white bg-[#FF9119] hover:bg-[#FF9119]/80 focus:ring-4 focus:outline-none focus:ring-[#FF9119]/50 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:hover:bg-[#FF9119]/80 dark:focus:ring-[#FF9119]/40 me-2 mb-2"
                           @endif >
                            <svg class="w-6 h-6 text-gray-800 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" viewBox="0 0 24 24">
                                <path fill-rule="evenodd" d="M14 7h-4v3a1 1 0 0 1-2 0V7H6a1 1 0 0 0-.997.923l-.917 11.924A2 2 0 0 0 6.08 22h11.84a2 2 0 0 0 1.994-2.153l-.917-11.924A1 1 0 0 0 18 7h-2v3a1 1 0 1 1-2 0V7Zm-2-3a2 2 0 0 0-2 2v1H8V6a4 4 0 0 1 8 0v1h-2V6a2 2 0 0 0-2-2Z" clip-rule="evenodd"/>
                            </svg>
                            @if($isAliConnected)
                                AliExpress is Connected
                            @else
                                Connect with AliExpress
                            @endif

                        </a>
                    </div>
                </main>
            </div>
        </div>
    </div>
@endsection
