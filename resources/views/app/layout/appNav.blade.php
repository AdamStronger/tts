<header>
    <nav class="fixed z-30 w-full bg-white border-b border-gray-200 dark:bg-gray-800 dark:border-gray-700 py-3 px-4">
        <div class="flex justify-between items-center max-w-screen-2xl mx-auto">
            <div class="flex justify-start items-center">
                <div class="flex mr-14">
                    <span class="self-center hidden sm:flex text-2xl font-semibold whitespace-nowrap dark:text-white">DropTTS</span>
                </div>

                <div class="hidden justify-between items-center w-full lg:flex lg:w-auto lg:order-1">
                    <ul class="flex flex-col mt-4 space-x-6 text-sm font-medium lg:flex-row xl:space-x-8 lg:mt-0">
                        <li>
                            <a href="{{ URL::tokenRoute('app.home') }}" class="block rounded text-primary-700 dark:text-primary-500" aria-current="page">Home</a>
                        </li>
                        <li>
                            <a href="{{ URL::tokenRoute('app.products') }}" class="block text-gray-700 hover:text-primary-700 dark:text-gray-400 dark:hover:text-white">Products List</a>
                        </li>
                        <li>
                            <a href="#" class="block text-gray-700 hover:text-primary-700 dark:text-gray-400 dark:hover:text-white">Profile</a>
                        </li>
                        <li>
                            <a href="{{ URL::tokenRoute('app.setting') }}" class="block text-gray-700 hover:text-primary-700 dark:text-gray-400 dark:hover:text-white">Settings</a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </nav>
</header>
