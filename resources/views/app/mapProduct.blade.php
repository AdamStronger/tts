@extends('shopify-app::layouts.default')

@section('styles')
    @vite(['resources/css/app.css','resources/js/app.js'])
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
@endsection

@section('content')
    <div class="bg-gray-50 dark:bg-gray-800">
        @include('app.layout.appNav')
        <div class="flex pt-16 overflow-hidden bg-gray-50 dark:bg-gray-900">
            <div class="relative w-full max-w-screen-2xl mx-auto h-full overflow-y-auto bg-gray-50 dark:bg-gray-900">
                <main>

                        <div class="p-4 bg-white border border-gray-200 rounded-lg shadow-sm dark:border-gray-700 sm:p-6 dark:bg-gray-800">

                            <form  id='SearchProduct' class="max-w-md mx-auto">
                                <label for="default-search" class="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Search</label>
                                <div class="relative">
                                    <div class="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                                        <svg class="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"/>
                                        </svg>
                                    </div>
                                    <input type="hidden"  id="store" name ='store' value="{{ Auth::user()->name}}">
                                    <input type="search" id="aliUrl" name ='aliUrl' class="block w-full p-4 ps-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Aliexpress product URL" required />
                                    <button type="submit" class="text-white absolute end-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Search</button>
                                </div>
                            </form>

                            <form id="MapProduct" action="{{ url("app/mapProduct") }}" method="POST">
                                <input type="hidden" name="store_map" value="{{ Auth::user()->name}}">
                                <input type="hidden" name="spProduct_id" value="{{$data['productID']}}">
                                <input type="hidden" name="aliProduct_id" id="aliProduct_id" value="">
                            <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
                                <div class="flex items-center justify-between flex-column md:flex-row flex-wrap space-y-4 md:space-y-0 py-4 bg-white dark:bg-gray-900">
                                </div>
                                <table class="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                                    <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                                    <tr id="header-row">
                                        <th scope="col" class="px-6 py-3">
                                            Id
                                        </th>
                                        <th scope="col" class="px-6 py-3">
                                            Product
                                        </th>
                                        <th scope="col" class="px-6 py-3">
                                            is Mapped
                                        </th>
                                        @if(array_key_exists('isMapped', $data['container']['variants'][0]))
                                            @foreach ($data['container']['variants'][0]['map']['variants'] as $var)
                                                <th scope="col" class="px-6 py-3">
                                                    {{$var}}
                                                </th>
                                            @endforeach
                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($data['container']['variants'] as $variant)

                                        <tr
                                            @if( $loop->index % 2 == 0 )
                                                class="bg-white dark:bg-gray-900  border-b dark:border-gray-700"
                                            @else
                                                class="bg-gray-100 dark:bg-gray-800 border-b dark:border-gray-700"
                                            @endif
                                            id="sku_container_{{$variant['id']}}">
                                            <td class="px-6 py-4">
                                                {{$variant['id']}}
                                            </td>
                                            <td class="flex items-center px-6 py-4 text-gray-900 whitespace-nowrap dark:text-white">
                                                <div class="ps-3">
                                                    <div class="text-base font-semibold">{{$variant['title']}}</div>
                                                </div>
                                            </td>
                                            @if(array_key_exists('isMapped', $variant))
                                                        {{var_dump($variant['map']['SelectedValue'])}}
                                            @endif
                                            <td>
                                                {{$variant['isMapped']}}
                                            </td>
                                        </tr>

                                    @endforeach

                                    @if(array_key_exists('isMapped', $data['container']['variants'][0]))

                                            <div scope="col" class="px-6 py-3">
                                                {{var_dump($data['container']['variants'][0]['map']['AliProduct'])}}
                                            </div>

                                    @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>


                        <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Save</button>
                    </form>
            </main>
        </div>
    </div>

        <script type="text/javascript">
            $(document).ready(function(){
                $('#MapProduct').on('submit', function(e) {
                    e.preventDefault();
                    $.ajax({
                        url: $(this).attr('action'),
                        method: $(this).attr('method'),
                        data: $(this).serialize(),
                        success: function(response) {
                            console.log(response)
                            // Optionally handle success response
                        },
                        error: function(xhr, status, error) {
                            alert('An error occurred: ' + xhr.responseText);
                            // Optionally handle error response
                        }
                    });
                });

                $('#SearchProduct').on('submit', function(e){
                    e.preventDefault(); // Prevent the default form submission
                    var aliUrl = $('#aliUrl').val();
                    var store = $('#store').val();
                    $.ajax({
                        url: '{{ url("app/aliproduct") }}',
                        type: 'POST',
                        data: {
                            aliUrl: aliUrl,
                            store : store
                        },
                        success: function(response) {
                            const skuList = response.product.Sku_list[0];
                            $('#aliProduct_id').val(response.product.product_id);

                            // Get unique SKU types
                            const skuTypes = [...new Set(skuList.map(sku => sku.sku_type))];

                            // Populate the table headers
                            const headerRow = $('#header-row');
                            headerRow.empty();
                            headerRow.append('<th scope="col" class="px-6 py-3">Id</th>');
                            headerRow.append('<th scope="col" class="px-6 py-3">Product</th>');

                            skuTypes.forEach(type => {
                                headerRow.append(`<th>${type}</th>`);
                            });

                            // Create td elements for each SKU type inside all tr elements with IDs that start with sku_container_
                            $('tr[id^="sku_container_"]').each(function() {
                                const skuContainer = $(this);
                                const skuId = skuContainer.attr('id').split('_')[2];
                                skuContainer.find('td:gt(1)').remove();
                                // Extract the SKU ID from the ID attribute
                                skuTypes.forEach(type => {
                                    let cellContent = `
                    <div class="custom-select relative">
                        <div class="select-selected p-2 border border-gray-300 rounded-md bg-white cursor-pointer flex items-center justify-between">Select an option</div>
                        <div class="select-items absolute bg-white border border-gray-300 rounded-md mt-1 shadow-lg z-10 w-full hidden max-h-56 overflow-y-auto">
                `;
                                    let selectContent = `<select name="${skuId}_${type}" class="hidden-select hidden">`;
                                    selectContent += '<option value="" disabled selected>Select an option</option>';

                                    skuList.forEach(sku => {
                                        if (sku.sku_type === type) {
                                            let imageContent = sku.sku_image ? `<img class="w-7 h-7 text-sky-500 inline-block mr-2" src="${sku.sku_image}" alt="${sku.sku_name}">` : '';
                                            cellContent += `
                            <div class="p-2 hover:bg-gray-100 cursor-pointer flex items-center" data-value="${skuId}_${sku.sku_name}">
                                ${imageContent} ${sku.sku_name}
                            </div>`;
                                            selectContent += `
                            <option value="${skuId}_${sku.sku_name}">
                                ${sku.sku_name}
                            </option>`;
                                        }
                                    });

                                    cellContent += '</div></div>';
                                    selectContent += '</select>';
                                    skuContainer.append(`<td>${cellContent}${selectContent}</td>`);
                                });
                            });

                            // Add event listeners for custom dropdown
                            const customSelects = document.querySelectorAll('.custom-select');
                            customSelects.forEach(select => {
                                const selected = select.querySelector('.select-selected');
                                const items = select.querySelector('.select-items');
                                const options = items.querySelectorAll('div');
                                const hiddenSelect = select.nextElementSibling;

                                selected.addEventListener('click', function() {
                                    closeAllSelect(this);
                                    items.classList.toggle('hidden');
                                    this.classList.toggle('select-arrow-active');
                                });

                                options.forEach(option => {
                                    option.addEventListener('click', function() {
                                        const value = this.getAttribute('data-value');
                                        const label = this.innerHTML;
                                        selected.innerHTML = label;
                                        hiddenSelect.value = value;
                                        items.classList.add('hidden');
                                        selected.classList.remove('select-arrow-active');
                                    });
                                });
                            });

                            function closeAllSelect(el) {
                                const items = document.querySelectorAll('.select-items');
                                const selected = document.querySelectorAll('.select-selected');
                                items.forEach(item => {
                                    if (item !== el) {
                                        item.classList.add('hidden');
                                    }
                                });
                                selected.forEach(sel => {
                                    if (sel !== el) {
                                        sel.classList.remove('select-arrow-active');
                                    }
                                });
                            }

                            document.addEventListener('click', function(e) {
                                if (!e.target.matches('.select-selected')) {
                                    closeAllSelect(null);
                                }
                            });
                        },
                        error: function(xhr) {
                            // Handle the error
                            console.log(xhr.responseText);
                            // You can display an error message here
                        }
                    });
                });
            });
        </script>
@endsection
