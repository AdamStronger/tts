<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('product_maps', function (Blueprint $table) {
            $table->id();
            $table->String('Store');
            $table->String('spTitle')->nullable();
            $table->String('spProductID')->nullable();
            $table->String('spProductSKU');
            $table->String('aliProductSKU')->nullable();
            $table->String('aliTitle')->nullable();
            $table->String('aliVariants')->nullable();
            $table->String('aliPoster_url')->nullable();
            $table->String('aliSKU_stock')->nullable();
            $table->String('aliSKU_price')->nullable();
            $table->String('aliOffer_sale_price')->nullable();
            $table->String('aliSKU_available_stock')->nullable();
            $table->String('aliSKU_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('product_maps');
    }
};
