<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('aliauths', function (Blueprint $table) {
            $table->id();
            $table->integer('userId');
            $table->String('authCode');
            $table->String('access_token');
            $table->String('expire_time');
            $table->string('refresh_token_valid_time');
            $table->string('refresh_token');
            $table->string('refresh_expires_in');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('aliauths');
    }
};
