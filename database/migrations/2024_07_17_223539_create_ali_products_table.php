<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ali_products', function (Blueprint $table) {
            $table->id();
            $table->String('ProductSKU');
            $table->String('Title');
            $table->String('Poster_url')->nullable();
            $table->String('SKU_stock')->nullable();
            $table->String('SKU_price')->nullable();
            $table->String('Offer_sale_price')->nullable();
            $table->String('SKU_available_stock')->nullable();
            $table->String('SKU_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ali_products');
    }
};
