<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->String('store');
            $table->string('spOrderId');
            $table->string('aliOrder');
            $table->string('AliTrackingNumber')->nullable();
            $table->string('AliTrackingNumberNext')->nullable();
            $table->string('fakeTrackingNumber')->nullable();
            $table->string('State');
            $table->string('City');
            $table->string('AliOrderStatus');
            $table->string('AliLogisticsStatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
