<?php
namespace App\Jobs;

use App\Models\AliAuth;
use App\Models\Order;
use APP\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Osiset\ShopifyApp\Objects\Values\ShopDomain;
use stdClass;
use App\IOP\iop\IopClient;
use App\IOP\iop\IopRequest;

class OrdersCreateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Shop's myshopify domain
     *
     * @var ShopDomain|string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;

    /**
     * Create a new job instance.
     *
     * @param string   $shopDomain The shop's myshopify domain.
     * @param stdClass $data       The webhook data (JSON decoded).
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    private function get_product_details($field , $sku) :String
    {
        $product = DB::table('product_maps')->where('spProductSKU',  $sku)->first();
        $result = '';
        if($field === 'product_id'){
            $result = $product->aliProductSKU;
        } elseif($field === 'sku_attr'){
            $result = $product->aliSKU_code;
        }
        return $result;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {

        $store = $this->shopDomain;
        // Convert domain
        $this->shopDomain = ShopDomain::fromNative($this->shopDomain);
        $order_data = (array)$this->data;

        $user = DB::table('users')->where('name',  $store)->first();
        $aliAuth =  DB::table('aliauths')->where('userId', $user->id)->first();

        $url = "https://api-sg.aliexpress.com/sync";

        $appkey = env('ALI_APPKEY');
        $appSecret = env('ALI_APPSECRET');

        $accessToken = $aliAuth->access_token;;

        $c = new IopClient($url, $appkey, $appSecret);

        $request = new IopRequest('aliexpress.ds.order.create');

        $request->addApiParam('ds_extend_request',
                    '{"payment":{"pay_currency":"USD"}}');

        $logistics_address = [
            "address" => $order_data['shipping_address']->address1,
            "city" => $order_data['shipping_address']->city,
            "country" => $order_data['shipping_address']->country_code,
            "full_name" => $order_data['shipping_address']->name,
            "contact_person" => $order_data['shipping_address']->name,
            "province" => $order_data['shipping_address']->province,
            "zip" => $order_data['shipping_address']->zip,
            "phone_country"=> "+1"
        ];

        if($order_data['shipping_address']->phone){
            $logistics_address['mobile_no'] = str_replace('+1', '' , $order_data['shipping_address']->phone);
        }

        $product_items = [];
        // Create the associative array for product_items
        foreach($order_data['line_items'] as $item){
            $product_items['product_count'] = $item->quantity;
            $product_items['product_id'] = $this->get_product_details('product_id', $item->variant_id);
            $product_items['sku_attr'] = $this->get_product_details('sku_attr', $item->variant_id);
        }
        $product_items['product_price']['currency_code'] = "USD";

        // Combine both arrays into a single array
        $param_place_order_request4_open_api_d_t_o = [
            "logistics_address" => $logistics_address,
            "product_items" => $product_items
        ];

        $order_string = json_encode($param_place_order_request4_open_api_d_t_o, JSON_UNESCAPED_SLASHES);
        $request->addApiParam('param_place_order_request4_open_api_d_t_o', $order_string);
        $response = $c->execute($request, $accessToken);
        $response_array = json_decode($response, true);
        if($response_array['aliexpress_ds_order_create_response']['result']['is_success']){
            foreach ($response_array['aliexpress_ds_order_create_response']['result']['order_list']['number'] as $number){
                $order = new Order(
                    [
                        'spOrderId' => $order_data['id'],
                        'aliOrder' => $number,
                        'City'  => $order_data['shipping_address']->city,
                        'State'  => $order_data['shipping_address']->province_code
                    ]
                );
                $order->save();
            }
        }
    }
}
