<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    public function show()
    {
        $user = Auth::user();
        $aliAuth =  DB::table('aliauths')->where('userId', $user->id)->first();
        $AliConnected = (bool)$aliAuth;
        return view("app.setting")->with('isAliConnected', $AliConnected);
    }
}
