<?php

namespace App\Http\Controllers;

use App\IOP\iop\IopClient;
use App\IOP\iop\IopRequest;
use Exception;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Http\Request;
use App\Models\Order;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Shopify\Auth\FileSessionStorage;
use Shopify\Clients\Graphql;
use Shopify\Context;
use stdClass;

define('SHOPIFY_APP_SECRET', env('SHOPIFY_APP_SECRET', 'a9ad4348abd9bd0ae9de6ebd9ed73fb65c26ac86ef6c63e0544bdfd43920fa9a'));

class ShopifyWebHooks extends Controller
{

    private function verify_webhook($data, $hmac_header)
    {
        $calculated_hmac = base64_encode(hash_hmac('sha256', $data, 'a9ad4348abd9bd0ae9de6ebd9ed73fb65c26ac86ef6c63e0544bdfd43920fa9a', true));
        return hash_equals($hmac_header, $calculated_hmac);
    }

    public function order_created()
    {
        $hmacHeader = request()->header('X-Shopify-Hmac-Sha256');
        $data = file_get_contents('php://input');
        $verified = $this->verify_webhook($data, $hmacHeader);
        $order_data = json_decode($data, true);

        if ($verified) {

            try {
                $isTik = false;

                foreach ($order_data['note_attributes'] as $note){
                    if ($note['name'] == 'Sales Channel' && $note['value'] == 'TikTok Shop - Teraboly'
                        && !str_contains($order_data['name'], '#')){
                        $isTik = true;
                        break;
                    }
                };

                if($order_data['shipping_address'] ===  null
                    || $order_data['closed_at'] != null
                    || $order_data['cancelled_at'] != null
                ) {
                    $isTik = false;
                }

                if($isTik){

                    $orderId = $order_data['id'];
                    $shopDomain = "efe6fb-ab.myshopify.com";
                    $accessToken = env('IFKEYSHOPIFY');
                    $apiVersion = '2024-10';

                    $endpoint = "https://{$shopDomain}/admin/api/{$apiVersion}/graphql.json";

                    $query = <<<'GRAPHQL'
                            mutation draftOrderCreateFromOrder($orderId: ID!) {
                              draftOrderCreateFromOrder(orderId: $orderId) {
                                draftOrder {
                                  id
                                }
                                userErrors {
                                  field
                                  message
                                }
                              }
                            }
                            GRAPHQL;

                    $variables = [
                        'orderId' => "gid://shopify/Order/{$orderId}"
                    ];

                    try {
                        $response = Http::withHeaders([
                            'Content-Type' => 'application/json',
                            'X-Shopify-Access-Token' => $accessToken,
                        ])->post($endpoint, [
                            'query' => $query,
                            'variables' => $variables,
                        ]);

                        $responseBody = $response->json();

                        if (isset($responseBody['errors'])) {
                            Log::error('GraphQL errors', $responseBody['errors']);
                            return null;
                        }

                        $draftOrderCreateFromOrder = $responseBody['data']['draftOrderCreateFromOrder'];

                        if (!empty($draftOrderCreateFromOrder['userErrors'])) {
                            Log::error('User errors', $draftOrderCreateFromOrder['userErrors']);
                            return null;
                        }

                        $this->completeDraftOrder($draftOrderCreateFromOrder['draftOrder']['id']);
                    } catch (\Exception $e) {
                        Log::error('Error creating draft order: ' . $e->getMessage());
                        return null;
                    }
                }
            } catch (Exception $e) {
               // Log::info('Error Catch : ' . $e);
                Log::error('Error Catch : ' . $e);
            }

        } else {
            // HMAC is invalid, return a 401 response
            http_response_code(401);
        }
    }

    private function completeDraftOrder($draftOrderId)
    {

        $shopDomain = "efe6fb-ab.myshopify.com";
        $accessToken = env('IFKEYSHOPIFY');
        $apiVersion = '2024-10';

        $endpoint = "https://{$shopDomain}/admin/api/{$apiVersion}/graphql.json";

        $query = <<<'GRAPHQL'
                mutation draftOrderComplete($id: ID!) {
                        draftOrderComplete(id: $id) {
                          draftOrder {
                            id
                            order {
                              id
                            }
                          }
                        }
                      }
                GRAPHQL;

        $variables = [
            'id' => $draftOrderId
        ];

        try {
            $response = Http::withHeaders([
                'Content-Type' => 'application/json',
                'X-Shopify-Access-Token' => $accessToken,
            ])->post($endpoint, [
                'query' => $query,
                'variables' => $variables,
            ]);

            $responseBody = $response->json();

            if (isset($responseBody['errors'])) {
                Log::error('GraphQL errors', $responseBody['errors']);
                return null;
            }

            $draftOrderComplete = $responseBody['data']['draftOrderComplete'];

            if (!empty($draftOrderComplete['userErrors'])) {
                Log::error('User errors', $draftOrderComplete['userErrors']);
                return null;
            }

        } catch (\Exception $e) {
            Log::error('Error completing draft order: ' . $e->getMessage());
            return null;
        }
    }

}
