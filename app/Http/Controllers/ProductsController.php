<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ProductsController extends Controller
{
    public function list()
    {
        $shop = Auth::user();
        $request = $shop->api()->rest('GET', '/admin/api/2024-07/products.json');
        $data = (array)$request['body']['products'];
        return view("app.products", compact('data'));
    }

    public function map(Request $request){
        $shop = Auth::user();
        $productId = $request->id;
        $request = $shop->api()->rest('GET', '/admin/api/2024-07/products/' . $productId . '.json');
        $data = (array)$request['body']['product'];
        $data['productID'] = $productId;
        $Mapped = (array)DB::table('product_maps')->where('spProductSKU', $data['container']['variants'][0]['id'])->first();
        $AliProduct =  (array)DB::table('ali_products')->where('ProductSKU', $Mapped['aliProductSKU'])->get();
        foreach($data['container']['variants'] as $key => $variant) {
            // Retrieve mapping information from the database
            $Mapped = (array)DB::table('product_maps')->where('spProductSKU', $variant['id'])->first();
            // Check if the mapping exists
            if (isset($Mapped['aliProductSKU'])) {
                $aliVariants = $Mapped['aliVariants']; // e.g., 'Color_Size'
                $aliTitle = $Mapped['aliTitle'];       // e.g., 'Blue_XS_'

                // Step 1: Split and process the data
                $attributes = array_map('trim', explode('_', strtolower($aliVariants)));
                $values = array_map('trim', explode('_', $aliTitle));

                // Step 2: Filter out any empty strings
                $attributes = array_filter($attributes, function($value) {
                    return $value !== '';
                });
                $values = array_filter($values, function($value) {
                    return $value !== '';
                });

                // Step 3: Create an associative array to hold only one "Color" and one "Size"
                $uniqueVariants = [];
                $uniqueValues = [];

                // Loop through attributes and ensure one value per unique attribute type
                foreach ($attributes as $index => $attribute) {
                    if (!isset($uniqueVariants[$attribute])) {
                        $uniqueVariants[$attribute] = $attribute;
                        $uniqueValues[$attribute] = $values[$index] ?? null; // Map the corresponding value

                        // Add to the global unique variants array
                        $allUniqueVariants[] = ucfirst($attribute);
                    }
                }

                // Step 4: Combine the unique variants with their respective values
                $result = $uniqueValues;

                // Step 5: Update the container with the result
                $data['container']['variants'][$key]['isMapped'] = true;
                $data['container']['variants'][$key]['map']['SelectedValue'] = $result;
                $data['container']['variants'][$key]['map']['variants'] = array_unique(array_keys($uniqueVariants));
                $data['container']['variants'][$key]['map']['AliProduct'] = $AliProduct;
            }
        }
        return view("app.mapProduct", compact('data'));
    }
}

