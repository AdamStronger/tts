<?php

namespace App\Http\Controllers;

use App\IOP\iop\IopClient;
use App\IOP\iop\IopRequest;
use App\Models\AliAuth;
use App\Models\AliProduct;
use App\Models\ProductMap;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class AliexpressController extends Controller
{
    public function Connect($userId)
    {

        $callback_url = 'https://www.droptts.online/app/aliexpressAccess/' . $userId ;
        $appKey = env('ALI_APPKEY');
       $url = 'https://api-sg.aliexpress.com/oauth/authorize?response_type=code&force_auth=true&redirect_uri='
           . $callback_url . '&client_id=' . $appKey ;
        return redirect()->away($url);
    }

    /**
     * @throws \Exception
     */
    public function getCode(request $request): void
    {
        $uri = $request->path();
        $userId = explode('aliexpressAccess/', $uri);
        $authCode = $request->query('code');
        $url = "https://api-sg.aliexpress.com/rest/auth/token/create";
        $appkey = env('ALI_APPKEY');
        $appSecret = env('ALI_APPSECRET');

        // Create an IopClient object and pass in the API address, appkey and appSecret
        $client = new IopClient($url , $appkey, $appSecret);

        // Create an IopRequest object and pass in the API interface name and parameters
        $request = new IopRequest('/auth/token/create');
        $request->addApiParam("code", $authCode);

        echo json_encode($request);

        try {
            // Execute API request, using GOP protocol
            $response = $client->execute($request);

            $resData= json_decode($response, true);

            echo 'UsedId' . $userId[1];

            $aliAuth = new AliAuth(
                [
                    'userId' => $userId[1],
                    'authCode' => $authCode,
                    'refresh_token'  => $resData['refresh_token'],
                    'refresh_token_valid_time'  => $resData['refresh_token_valid_time'],
                    'refresh_expires_in'  => $resData['refresh_expires_in'],
                    'access_token'  => $resData['access_token'],
                    'expire_time' => $resData['expires_in']
                ]
            );
            $aliAuth->save();
            echo 'Aliexpress Connected successfully <br>';
            echo 'Record ID: ' . $aliAuth->id;
        } catch (Exception $e) {
            echo $e->getMessage();
            // Catch exception and print stack information
            info('Aliexpress request error' . $e->getMessage());
        }


    }

    public function getProduct(request $request){

        $request->validate([
            'aliUrl' => 'required|url',
            'store' => 'required|String'
        ]);

        // Extract the product ID from the URL
        $url = $request->input('aliUrl');
        $store = $request->input('store');
        $user = DB::table('users')->where('name', $store)->first();
        $aliAuth =  DB::table('aliauths')->where('userId', $user->id)->first();

        preg_match('/item\/(\d+)\.html/', $url, $matches);

        if (isset($matches[1])) {
            $itemId = $matches[1];

            $url = "https://api-sg.aliexpress.com/sync";
            $appKey = env('ALI_APPKEY');
            $appSecret = env('ALI_APPSECRET');
            $accessToken = $aliAuth->access_token;

            $c = new IopClient($url, $appKey, $appSecret);
            $request = new IopRequest('aliexpress.ds.product.get');

            $request->addApiParam('ship_to_country','US');
            $request->addApiParam('product_id',$matches[1]);
            $request->addApiParam('target_currency','USD');
            $request->addApiParam('target_language','en');

            try {

                $response = $c->execute($request, $accessToken);

                $product = [
                    'Title' => '',
                    'product_id' => '',
                    'product_img_list' => [],
                    'Sku_list'=> []

                ];
                //$response_data = (array)$response->aliexpress_ds_product_get_response->result;

                $response_array = json_decode($response, true);
                $response_data = $response_array['aliexpress_ds_product_get_response']['result'];

                if(array_key_exists('ae_multimedia_info_dto', $response_data)){
                    $product['product_img_list'] = explode(';', $response_data['ae_multimedia_info_dto']['image_urls']);
                }

                if(array_key_exists('ae_item_base_info_dto', $response_data)){
                    $product['Title'] = $response_data['ae_item_base_info_dto']['subject'];
                    $product['product_id'] = $response_data['ae_item_base_info_dto']['product_id'];
                }

                $mapped_sku_info = []; // Initialize the array to store unique SKUs
                $unique_names = []; // Initialize an array to keep track of unique SKU names
                $AliProduct = []; // Initialize
                if (isset($response_data['ae_item_sku_info_dtos']['ae_item_sku_info_d_t_o'])) {
                    foreach ($response_data['ae_item_sku_info_dtos']['ae_item_sku_info_d_t_o'] as $sku_info) {
                        $properties = $sku_info['ae_sku_property_dtos']['ae_sku_property_d_t_o'];

                        $AliProduct['SKU_code'] = $sku_info['sku_attr'];
                        $AliProduct['ProductSKU'] = $product['product_id'];

                        $AliProductProperties = '';

                        foreach ($properties as $property) {
                            Log::info($property);
                            $sku_name = $property['property_value_definition_name'] ?? $property['sku_property_value'];
                            $sku_type = $property['sku_property_name'];
                            $AliProductProperties .= $sku_name . '_';

                            // Check if the SKU name is already in the unique_names array
                            if (!in_array($sku_name, $unique_names)) {
                                // Add the SKU name to the unique_names array
                                $unique_names[] = $sku_name;

                                // Initialize the SKU info array
                                $sku_info_array = [
                                    'sku_name' => $sku_name,
                                    'sku_type' => $sku_type
                                ];

                                // Check if 'sku_image' exists and add it to the SKU info array
                                if (isset($property['sku_image'])) {
                                    $sku_info_array['sku_image'] = $property['sku_image'];
                                    $AliProduct['Poster_url'] = $property['sku_image'] ;
                                }

                                // Add the SKU info to the mapped_sku_info array
                                $mapped_sku_info[] = $sku_info_array;
                            }
                        }

                        $exists = DB::table('ali_products')->where('SKU_code', $AliProduct['SKU_code'])->exists();
                        info('before product ali mapping');
                        info($exists);
                        if(!$exists){
                            info('inside product ali mapping');
                            $AliProduct['Title'] = $AliProductProperties;
                            info($AliProduct);
                            $newAliProduct = new AliProduct(
                                $AliProduct
                            );
                            $newAliProduct->save();
                        }
                    }
                    $product['Sku_list'][] = $mapped_sku_info;
                }

                return response()->json(['product' => $product], 200, [], JSON_UNESCAPED_SLASHES);

            } catch (Exception $e) {
                return response()->json(['Error Catch' => $e->getMessage()]);
            }
        } else {
            $error = "error";
            return response()->json(['error' => $error]);
        }
    }
    public function mapProduct(request $request){

        $data = $request->all();
        $result = [
            'product' => [
                'store' => '',
                'aliProduct_id' => '',
                'spProduct_id' => '',
                'sku_data' => []
            ]
        ];

        foreach ($data as $key => $value) {
            if ($key === 'store_map') {
                $result['product']['store'] = $value;
            } else if ($key === 'aliProduct_id') {
                $result['product']['aliProduct_id'] = $value;
            } else if ($key === 'spProduct_id') {
            $result['product']['spProduct_id'] = $value;
            } else {
                // Extract SKU and type
                list($sku, $type) = explode('_', $key, 2);
                $result['product']['sku_data'][$sku][$type] = str_replace($sku . '_', '', $value);
            }
        }

        $sku_data = $result['product']['sku_data'];
        $foundProducts = $this->findMatchingProducts($result['product']['aliProduct_id'], $sku_data);
        $mapped_key = '';
        foreach ($sku_data as $sku_id => $attributes) {
            $keys = array_keys($attributes);
            $mapped_key = implode('_', $keys);
            break;
        }

        foreach ($foundProducts as $sku => $product) {
            $productMap = new ProductMap(
                [
                    'Store'=> $result['product']['store'],
                    'spProductID' => $result['product']['spProduct_id'],
                    'aliVariants' => $mapped_key,
                    'spProductSKU' => $sku,
                    'aliProductSKU' => $result['product']['aliProduct_id'],
                    'aliSKU_code' => $product->SKU_code,
                    'aliPoster_url' => $product->Poster_url,
                    'aliTitle' => $product->Title
                ]
            );
            $productMap->save();
        }
        return response()->json([
            'result' => $result,
            'foundProducts' => $foundProducts
        ], 200, [], JSON_UNESCAPED_SLASHES);

    }
    private function findMatchingProducts($productId, $skuData)
    {
        $foundProducts = [];

        // Retrieve all products with the given product ID
        $products = DB::table('ali_products')->where('ProductSKU', $productId)->get();

        foreach ($skuData as $sku => $attributes) {
            foreach ($products as $product) {
                $match = true;
                $productAttributes = explode('_', $product->Title);

                foreach ($attributes as $value) {
                    if (!in_array($value, $productAttributes, true)) {
                        $match = false;
                        break;
                    }
                }

                if ($match) {
                    $foundProducts[$sku] = $product;
                    break;
                }
            }
        }
        return $foundProducts;
    }
}

