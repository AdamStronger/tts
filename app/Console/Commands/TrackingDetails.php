<?php

namespace App\Console\Commands;

use App\IOP\iop\IopClient;
use App\IOP\iop\IopRequest;
use App\Models\Order;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TrackingDetails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'TrackingDetails:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        Log::info("Cron Job running at ". now());
        $orders = DB::table('orders')
            ->where('AliLogisticsStatus', 'SELLER_SEND_GOODS')
            ->get();
        $url = "https://api-sg.aliexpress.com/sync";
        $appkey = env('ALI_APPKEY');
        $appSecret = env('ALI_APPSECRET');

        foreach ($orders as $order){
            $user = DB::table('users')->where('name',  $order->store)->first();
            $aliAuth =  DB::table('aliauths')->where('userId', $user->id)->first();
            if($aliAuth->access_token){

                $c =  new IopClient($url,$appkey,$appSecret);
                $request = new IopRequest('aliexpress.ds.order.tracking.get');
                $request->addApiParam('ae_order_id',$order->id);
                $request->addApiParam('language','en_US');
                $response = $c->execute($request, $aliAuth->access_token);
                $response_array = json_decode($response, true);

                $item = Order::find($order->id);
                if(
                    isset($response_array['aliexpress_ds_order_tracking_get_response']['result']['data']
                        ['tracking_detail_line_list']['tracking_detail_line_list']['tracking_detail']['mail_no'])
                )
                {
                    $item->AliTrackingNumberNext = $response_array['aliexpress_ds_order_tracking_get_response']['result']['data']
                    ['tracking_detail_line_list']['tracking_detail_line_list']['tracking_detail']['mail_no'];
                }

                $item->save();

            }
        }
    }
}
