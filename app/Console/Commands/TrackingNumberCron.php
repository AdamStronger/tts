<?php

namespace App\Console\Commands;

use App\Models\Order;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\IOP\iop\IopClient;
use App\IOP\iop\IopRequest;

class TrackingNumberCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'TrackingNumber:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        Log::info("Cron Job running at ". now());
        $orders = DB::table('orders')
            ->where('AliLogisticsStatus', 'New')
            ->get();
        $url = "https://api-sg.aliexpress.com/sync";
        $appkey = env('ALI_APPKEY');
        $appSecret = env('ALI_APPSECRET');

        foreach ($orders as $order){
            $user = DB::table('users')->where('name',  $order->store)->first();
            $aliAuth =  DB::table('aliauths')->where('userId', $user->id)->first();
            if($aliAuth->access_token){
                $c = new IopClient($url,$appkey,$appSecret);
                $request = new IopRequest('aliexpress.trade.ds.order.get');
                $request->addApiParam('single_order_query','{"order_id":"'.$order->id .'"}');
                $request->addApiParam('language','en_US');
                $response = $c->execute($request, $aliAuth->access_token);

                $response_array = json_decode($response, true);
                $item = Order::find($order->id);

                if(isset($response_array['aliexpress_trade_ds_order_get_response']['result']['order_status'])){
                    $item->AliOrderStatus = $response_array['aliexpress_trade_ds_order_get_response']['result']['order_status'];
                }
                if(isset($response_array['aliexpress_trade_ds_order_get_response']['result']['logistics_status'])){
                    $item->AliLogisticsStatus = $response_array['aliexpress_trade_ds_order_get_response']['result']['logistics_status'];
                }

                $item->save();
            }
        }
    }
}
