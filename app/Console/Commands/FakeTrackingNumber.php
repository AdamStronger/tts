<?php

namespace App\Console\Commands;

use App\Models\Order;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class FakeTrackingNumber extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'FakeTrackingNumber:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        Log::info("Cron Job running at ". now());
        $orders = DB::table('orders')
            ->where('AliLogisticsStatus', 'SELLER_SEND_GOODS')
            ->where('created_at', '>=', Carbon::now()->subHours(40))
            ->whereNotNull('fakeTrackingNumber')
            ->get();
        $url = env('TACO_URL');
        $apiKey = env('TACO_X_API_KEY');

        foreach ($orders as $order){
            $createdAt = $order->created_at;
            $now = Carbon::now();
            $businessHours = $this->getBusinessHoursDifference($createdAt, $now);

            if ($businessHours >= 40) {

                $data = [
                    'city' => $order->city,
                    'state' =>$order->state,
                    'deliveryState' => ["in-transit"],
                    'minShippedAt' => Carbon::parse($order->created_at)->timestamp * 1000,
                    'from' => now()->addDays(2)->getTimestampMs(),
                    'to' => now()->addDays(6)->getTimestampMs()
                ];

                $response = Http::withHeaders([
                    'x-api-key' => $apiKey,
                    'Content-Type' => 'application/json',
                ])->post($url, $data);

                $response_array = json_decode($response, true);

                $item = Order::find($order->id);

                if(isset($response_array['error'])){
                    $item->fakeTrackingNumber = $response_array['trackingNr'];
                    $item->save();
                }
            }

        }
    }

    private function getBusinessHoursDifference($start, $end) {
        $start = Carbon::parse($start);
        $end = Carbon::parse($end);

        // Initialize business hours
        $businessHours = 0;

        // Loop from start to end date
        while ($start->lessThan($end)) {
            // Check if the current day is a weekend
            if (!$start->isWeekend()) {
                // If not a weekend, add the business hours for the day
                $businessHours += $start->diffInHours($end->copy()->endOfDay(), false);
            }

            // Move to the next day
            $start->addDay()->startOfDay();
        }

        return $businessHours;
    }

}
