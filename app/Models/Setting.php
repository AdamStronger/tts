<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $array)
 */
class Setting extends Model
{
    use HasFactory;

    public String $userId;
    public String $isAliexpressConnected;
    public String $AliAuthId;

    protected $table = 'settings';
    protected $fillable = ['userId', 'isAliexpressConnected', 'AliAuthId'];
}
