<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductMap extends Model
{
    use HasFactory;

    public String $Store;
    public String $spTitle;
    public String $spProductID;
    public String $aliTitle;
    public String $aliVariants;
    public String $spProductSKU;
    public String $aliProductSKU;
    public String $aliPoster_url;
    public String $aliSKU_stock;
    public String $aliSKU_price;
    public String $aliOffer_sale_price;
    public String $aliSKU_available_stock;
    public String $aliSKU_code;

    protected $table = 'product_maps';
    protected $fillable = ['Store', 'spProductID', 'aliTitle', 'spTitle','spProductSKU', 'aliProductSKU', 'aliVariants', 'aliPoster_url', 'aliSKU_stock', 'aliSKU_price', 'aliOffer_sale_price', 'aliSKU_available_stock', 'aliSKU_code'];
}
