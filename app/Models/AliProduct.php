<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AliProduct extends Model
{
    use HasFactory;

    public String $ProductSKU;
    public String $Title;
    public String $Poster_url;
    public String $SKU_stock;
    public String $SKU_price;
    public String $Offer_sale_price;
    public String $SKU_available_stock;
    public String $SKU_code;

    protected $table = 'ali_products';

    protected $fillable = ['ProductSKU', 'Title', 'Poster_url', 'SKU_stock', 'SKU_price', 'Offer_sale_price', 'SKU_available_stock', 'SKU_code'];


}
