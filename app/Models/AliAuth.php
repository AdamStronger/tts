<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $array)
 */
class AliAuth extends Model
{
    use HasFactory;

    public String $userId;
    public String $authCode;
    public String $refresh_token_valid_time;
    public String $refresh_token;
    public String $refresh_expires_in;
    public String $access_token;
    public String $expires_in;

    protected $table = 'aliauths';
    protected $fillable = ['userId', 'authCode', 'refresh_token_valid_time', 'refresh_token', 'refresh_expires_in', 'access_token', 'expire_time'];
}
