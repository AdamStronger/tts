<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $array)
 */
class Order extends Model
{
    use HasFactory;

    public String $store;
    public String $spOrderId;
    public String $aliOrder;
    public String $AliTrackingNumber;
    public String $AliTrackingNumberNext;
    public String $fakeTrackingNumber;
    public String $AliOrderStatus;
    public String $AliLogisticsStatus;
    public String $City;
    public String $State;

    protected $table = 'orders';
    protected $fillable = ['store', 'spOrderId', 'aliOrder', 'AliTrackingNumber','AliTrackingNumberNext',
        'fakeTrackingNumber','$AliOrderStatus', '$AliLogisticsStatus', 'City', 'State'];
}
